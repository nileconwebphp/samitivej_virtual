function SubmitPayment() {
	var amount = $('input[name=lhid]').val();
	var trans_id = url_query('trans_id');
	var timestamp = moment().format('YYYYMMDDHHmmss');

	var pay = $('.col_select_payment.pay_active').attr('data-method');
	if(pay === '' || pay === undefined) {
		$('#paymentNotSelect .modal-body h3').html('กรุณาเลือกวิธีการชำระเงิน');
		$('#paymentNotSelect').modal();
	} else {
		if(pay === "DR") {
			var callback_url = "&backURL=https://uatvirtual.samitivejhospitals.com/WebApplication/gwscb/returndirectdebit.aspx";
			var usid = $('input[name=usid]').val();
			var text = 'payee_id=00001&cust_id='+ usid +'&ref_no='+ trans_id +'&ref_date='+timestamp+'&amount='+ amount +'&currency=THB&description=Samitivej Virtual Hospital Phone Call Test&usrdat=Pay At '+ timestamp +'&dueDate='+moment().format('YYYYMMDD');
			let encryptext = enctext(text);
			location.href="https://nsips-test.scb.co.th:443/NSIPSWeb/NsipsMessageAction.do?profile_id=1000012171&terminal=22119254&command=WPDBAC&enctext="+ encryptext + callback_url;
		}
		else if (pay === "SCBEASY") {
			location.href="payment_sumtotal.html";
		}
		else if(pay === "ALWP") {
			var vr_orderid = url_query('trans_id')
			location.href="alipay_qr.html?qrtype=A&trans_id="+vr_orderid;
		}
		else if(pay === "WP") {
			var vr_orderid = url_query('trans_id')
			location.href="alipay_qr.html?qrtype=W&trans_id="+vr_orderid;
		}
		else if(pay === "CP") {
			directpay();
		} 
		else if(pay === "QR") {
			// tqrcpay()
			location.href="scb_pay.html?trans_id="+ url_query('trans_id');
		}
		else if(pay === "QRCS") {
			location.href="scb_pay.html?trans_id="+ url_query('trans_id');	
		}
		else {
			return false;
		}
	}
}

function tqrcpay(ref1, ref2) {
	var Biller_ID = '0115010753600004881';
	var Reference1 = "02"+ padLeft(ref1.length, 2).replace('-','') + ref1;
	var Reference2 = "03"+ padLeft(ref2.length, 2).replace('-','') + ref2;
	var aid = '0016A000000677010112';

	var Amount = '5406500.00'; // amount baht
	var currency = '5303764'; // currency code
	var lang = '5802TH'; // lang
	// var terminalid = '62SCB001';
	var version = '000201'; // qr version
	var qrmethod = '010212'; // qr method

	var summerchantlength = aid.length + Biller_ID.length + Reference1.length + Reference2.length;

	var merchant_info = '30'+summerchantlength+aid+Biller_ID+Reference1+Reference2;
	var checksum = '6304';
	var tqr = version+qrmethod+merchant_info+currency+Amount+lang+checksum;
	var crc = crcccittt(tqr);
	return tqr + crc;
}



function qrcsPay(transcode, price, dataid) {
	var resp;
	console.log(price)
	$.ajax({
		url: baseurl+'CreateQRCS',
		type: "post",
		async: false,
		data: {
			qrCodeType: "EM",
			merchantID: "010000000000907321",
			terminalID: 77701385,
			invoice: transcode,
			amount: price,
			extExpiryTime: 1440,
			userDefined: transcode,
			note: transcode,
		},
		success: function (response, xhr) {
			if(response.IsSuccess === true) {
				resp = response.data;
				updateQRIDwithOrder(resp.qrCodeID, dataid);
			} else {
				resp = false;
			}
		},
		error: function(jqXHR, textStatus, errorThrown) {
			console.log(textStatus, errorThrown);
		}
	});
	return resp;
}

function updateQRIDwithOrder(qrid, transcode) {
	$.ajax({
		url: baseurl+'AddQRByOrderId',
		type: "post",
		data: {
			OrderId: transcode,
			QRId: qrid
		},
		success: function (response, xhr) {
			if(response.IsSuccess === true) {
				return true;
			} else {
				return false;
			}
		},
		error: function(jqXHR, textStatus, errorThrown) {
			console.log(textStatus, errorThrown);
		}
	});
}

function checkQrcsStatusTrue(qrid) {
	var resp;
	$.ajax({
		url: baseurl+'CheckQRPayment',
		type: "post",
		async: false,
		data: {
			QRId: qrid
		},
		success: function (response, xhr) {
			resp = response.IsSuccess;
		},
		error: function(jqXHR, textStatus, errorThrown) {
			console.log(textStatus, errorThrown);
		}
	});

	return resp;
}

function returnOrderTransaction(tsid) {
    var dataresp;

    $.ajax({
        url: baseurl+'OrderDetail',
        type: "post",
        async: false,
        data: {
            TransactionCode: tsid
        },
        success: function (response, xhr) {
            if(response.IsSuccess === true) {
                var resp = response.data.TransactionCode;
                var price = response.data.Price;
                dataresp = {
                	"transcode": resp,
                	"price": price,
                	"orderid": response.data.OrderId
                }
            } else {
                $('#paymentNotSelect .modal-body h3').html('ไม่พบรายการสั่งซื้อดังกล่าว กรุณาลองอีกครั้ง');
                $('#paymentNotSelect').modal();
                return false;
            }
        },
        error: function(jqXHR, textStatus, errorThrown) {
           console.log(textStatus, errorThrown);
        }
    });

    return dataresp;
}

function aliwechat_pay() {
	var data = '';
	var type = url_query('qrtype');
	var vr_orderid = url_query('trans_id');
	if(type === 'A') {
		type = 'A';
	} else {
		type = 'W';
	}

	$.ajax({
		url: baseurl+'CreateAliPayWeChat',
		type: "post",
		async: false,
		data: {
			tran_type: type,
			out_tradeno: vr_orderid,
			terminal_id: "T1000201",
			company_id: "001",
			total_fee: 500.00
		},
		success: function (response, xhr) {
			data = response.data.STATUS[0];
			if(data.message === "Success") {
				if(type === 'W') {
					data = 'https://api.qrserver.com/v1/create-qr-code/?data='+data.code_url;
				} else {
					data = data.code_url;
				}
			} else {
				data = false;
			}
		},
		error: function(jqXHR, textStatus, errorThrown) {
			console.log(textStatus, errorThrown);
		}
	});

	return data;
}

function enctext(text) {
	var enctext = '';

	$.ajax({
		url: baseurl+'CreateEnc',
		type: "post",
		async: false,
		data: {
			InputText: text
		},
		success: function (response, xhr) {
			enctext = response.data;
		},
		error: function(jqXHR, textStatus, errorThrown) {
			console.log(textStatus, errorThrown);
		}
	});
	
	return enctext;
}

function padLeft(nr, n, str){
	return Array(n-String(nr).length+1).join(str||'0')+nr;
}

function directpay() {
	//Merchant's account information
	var merchant_id = "764764000001840";		//Get MerchantID when opening account with 2C2P
	var secret_key = "847F83253BFC02336B3A4AAE4F2B818DD83B1BD5F423FB1F90FF371C556D2D57";	//Get SecretKey from 2C2P PGW Dashboard
	
	//Transaction information
	var payment_description  = 'Samitivej Virtual Hospital Phone Call Test';
	var order_id  = url_query('trans_id');
	var currency = "764";
	var amount = $('input[name=lhid]').val()+'00';
	amount  = padLeft(amount,12);
	var payment_option = 'C';
	var default_lang = 'th';

	//Request information
	var version = "6.3";	
	var payment_url = "https://demo2.2c2p.com/2C2PFrontEnd/RedirectV3/payment";
	var result_url_1 = "https://uatvirtual.samitivejhospitals.com/WebApplication/payment/Payment.aspx";

	//Construct signature string
	// var params = version+merchant_id+payment_description+order_id+currency+amount+result_url_1;
	var params = "6.3JT01SparEcomerce260225621032764000000050000https://uatvirtual.samitivejhospitals.com/WebApplication/payment/Payment.aspx";

	var hash_value = CryptoJS.HmacSHA1(params, secret_key);

	var html = '<form id="myform" method="post" action="'+ payment_url +'" style="visibility: hidden">'
	+'<input type="hidden" name="version" value="'+ version +'"/>'
	+'<input type="hidden" name="merchant_id" value="'+ merchant_id +'"/>'
	+'<input type="hidden" name="currency" value="'+ currency +'"/>'
	+'<input type="hidden" name="result_url_1" value="'+ result_url_1 +'"/>'
	+'<input type="hidden" name="hash_value" value="'+ hash_value +'"/>'
	+'<input type="hidden" name="payment_description" value="'+ payment_description +'"  readonly/><br/>'
	+'<input type="hidden" name="order_id" value="'+ order_id +'"  readonly/><br/>'
	+'<input type="hidden" name="amount" value="'+ amount +'" readonly/><br/>'
	+'<input type="hidden" name="payment_option" value="'+ payment_option +'"/>'
	+'<input type="hidden" name="default_lang" value="'+ default_lang +'" readonly/><br/>'
	+'<input type="submit" name="submit" id="confirmpay" value="Confirm" />'
	+'</form>';

	$('body').append(html);
	// $('#confirmpay').click();
}

function crcccittt(ascii) {
	var enctext;
	$.ajax({
		url: baseurl+'CRC',
		type: "post",
		async: false,
		data: {
			ASCIIInput: ascii
		},
		success: function (response, data, xhr) {
			if(response.IsSuccess === true) {
				enctext = response.ErrMessage;
			} else {
				enctext = false;
			}
		},
		error: function(jqXHR, textStatus, errorThrown) {
			console.log(textStatus, errorThrown);
		}
	});
	return enctext;
}

function checkInquiry(transid, type) {
	$.ajax({
		url: baseurl+'CreateSVVHInquiry',
		type: "post",
		// async: false,
		data: {
			tran_type: type,
			out_tradeno: transid,
			terminal_id: 'T1000201',
			company_id: 001
		},
		success: function (response, xhr) {
			resp = response.data.STATUS[0];
			if(resp === true) {
				location.href="payment_sumtotal_.html?trans_id="+transid;
			} else {
				return false;
			}
		},
		error: function(jqXHR, textStatus, errorThrown) {
			console.log(textStatus, errorThrown);
		}
	});
}