/* eslint-disable no-unused-vars */
// Make a copy of this file and save it as config.js (in the js directory).

// Set this to the base URL of your sample server, such as 'https://your-app-name.herokuapp.com'.
// Do not include the trailing slash. See the README for more information:

var SAMPLE_SERVER_BASE_URL = 'http://localhost:9000/';

// OR, if you have not set up a web server that runs the learning-opentok-php code,
// set these values to OpenTok API key, a valid session ID, and a token for the session.
// For test purposes, you can obtain these from https://tokbox.com/account.

var API_KEY = '46248152';
var SESSION_ID = url_query('OpenTokSessionId');
var TOKEN = url_query('OpenTokToken');
var TransactionId = url_query('TransactionId');
var TransactionStatus = url_query('TransactionStatus');
// var SESSION_ID = '2_MX40NjI0ODE1Mn5-MTU0NzYzMzMwMTM0OX5QYWRLNzFQT0NUWkNVRnVxTC9VMkVESHR-UH4';
// var TOKEN = 'T1==cGFydG5lcl9pZD00NjI0ODE1MiZzaWc9MmJhMmFlNGVhYzQ0ZmM4NjcwNWZlZDhmZjIzMWQwYzhjZjBmYzZkODpzZXNzaW9uX2lkPTJfTVg0ME5qSTBPREUxTW41LU1UVTBOell6TXpNd01UTTBPWDVRWVdSTE56RlFUME5VV2tOVlJuVnhUQzlWTWtWRVNIUi1VSDQmY3JlYXRlX3RpbWU9MTU0NzYzMzc3MCZub25jZT0wLjEwMjkzMzcwODI1MjAxMjUzJnJvbGU9cHVibGlzaGVyJmV4cGlyZV90aW1lPTE1NTAyMjU3NzAmaW5pdGlhbF9sYXlvdXRfY2xhc3NfbGlzdD0=';

