var environment = "test";
var baseurl = '';
var baseimg = '';

if(environment === "test") {
	baseurl = "https://uatvirtual.samitivejhospitals.com/WebService/api/";
	baseimg = "https://uatvirtual.samitivejhospitals.com/";
} else {
	baseurl = "https://uatvirtual.samitivejhospitals.com/WebService/api/";
	baseimg = "https://uatvirtual.samitivejhospitals.com/";
}

function formatNumber(x, currency) {
	var parts = x.toString().split(".");
	parts[0] = parts[0].replace(/\B(?=(\d{3})+(?!\d))/g, ",");
	return parts.join(".");
  }

  function datenow(d, m, y) {
	var thmonth = new Array ("","มกราคม","กุมภาพันธ์","มีนาคม","เมษายน","พฤษภาคม","มิถุนายน", "กรกฎาคม","สิงหาคม","กันยายน","ตุลาคม","พฤศจิกายน","ธันวาคม");
  
	return ("วันที่ "+ d + " " + thmonth[m]+ " " + (parseInt(y)+543));
  }

function url_query( query ) {
	query = query.replace(/[\[]/,"\\\[").replace(/[\]]/,"\\\]");
	var expr = "[\\?&]"+query+"=([^&#]*)";
	var regex = new RegExp( expr );
	var results = regex.exec( window.location.href );
	if ( results !== null ) {
		return results[1];
	} else {
		return false;
	}
}

