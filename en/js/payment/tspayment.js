function getOrderTransaction(tsid) {
    var resp;

    $.ajax({
        url: baseurl+'OrderDetail',
		type: "post",
		// async: false,
        data: {
			TransactionCode: tsid
		},
        success: function (response, xhr) {
            if(response.IsSuccess === true) {
                resp = response.data;
                var CallDate = resp.CreateDate;
                $('.number_hn').html(resp.TransactionCode);
                $('.result_date').html(datenow(moment(CallDate).format('D'),moment(CallDate).format('M'),moment(CallDate).format('Y')));
                $('.result_dr').html(resp.DoctorName + ' ('+ resp.DoctorSection +')');
                $('input[name=lhid]').val(resp.Price);
                $('.text_total_payment_num').html('฿' + formatNumber(resp.Price) + '.00');
            } else {
                $('#paymentNotSelect .modal-body h3').html('ไม่พบรายการสั่งซื้อดังกล่าว กรุณาลองอีกครั้ง');
                $('#paymentNotSelect').modal();
                return false;
            }
        },
        error: function(jqXHR, textStatus, errorThrown) {
           console.log(textStatus, errorThrown);
        }
    });
}